from bs4 import BeautifulSoup as Soup
import sys
from sys import path
from os import path
"""
Inserts to file
<meta name="format-detection" content="telephone=no">

"""

def insertEdgeFix(filep):
    htmlIn = open(filep, mode='r')
    html = htmlIn.read()
    if not containsEdgeFix(html):
        soup = Soup(html, 'html.parser')
        head = soup.find('head')
        meta = soup.new_tag('meta')
        meta['name'] = "format-detection"
        meta['content'] = "telephone=no"
        head.insert(1, meta)
        return soup
    else:
        sys.exit(0) # No need to fix

def saveChanges(filep, soup):
    #html = soup.prettify("utf-8")
    with open(filep, "wb") as filep:
        filep.write(str(soup))

def containsEdgeFix(htmlStr):
    return '<meta name="format-detection" content="telephone=no">' in htmlStr

def senseFilepaths():
    """
        C:\Program Files\Qlik\Sense\Client\client.html
        C:\Program Files\Qlik\Sense\Client\hub.html
    """
    root = path.join('Program Files', 'Qlik', 'Sense', 'Client')
    client = path.join(root, 'client.html')
    hub = path.join(root, 'hub.html')
    return client, hub

def edgeFix(files):
    for f in files:
        soup = insertEdgeFix(f)
        saveChanges(f, soup)
    print 'done!'

def main():
    files = senseFilepaths()
    edgeFix(files)

if __name__ == "__main__":
    main()
