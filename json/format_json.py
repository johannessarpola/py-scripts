import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-f', "--file", help="file",)

args = parser.parse_args()

source = open(args.file)
data = source.read()
size = len(data)
source.close()
parsed = json.loads(data);
out = open(args.file, "w")
print("Started formatting and writing json to file")
jsStr = json.dumps(parsed, indent=4, sort_keys=True)
out.write(jsStr);
print(f"Wrote total of {size} chars to new file")
out.close()
