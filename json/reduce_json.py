import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-f', "--field", help="field to leave from json",)
parser.add_argument('-o', "--origin", help="original file",)
parser.add_argument('-d', "--destination", help="destination file",)

args = parser.parse_args()

data = open(args.origin).readlines()
out = open(args.destination, "w")
print("Started writing to file")
max = len(data)
increment = 0
for row in data:
    element = json.loads(row)
    out.write(element[args.field])
    out.write("\n")
    increment = increment + 1

print(f"Wrote total of {increment} rows to new file")
out.close()
