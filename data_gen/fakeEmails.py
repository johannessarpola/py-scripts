import sys
import random
from random import choice
from string import ascii_lowercase

emails = [] 
header = 'Email'

if len(sys.argv) != 3:
    print("Use two arguments [filename] [count]")
    sys.exit()
print('Filename was '+str(sys.argv[1]))
print('Count was '+str(sys.argv[2]))

filename = sys.argv[1]
for j in range(int(sys.argv[2])):
    nameLen = random.randint(5, 20)
    domainLen = random.randint(5, 20)
    extensionLen = random.randint(2,3)
    name = ''.join(choice(ascii_lowercase) for i in range(nameLen))
    domain = ''.join(choice(ascii_lowercase) for i in range(domainLen))
    extensio = ''.join(choice(ascii_lowercase) for i in range(extensionLen))
    email = name+'@'+domain+'.'+extensio
    emails.append(email)

def listToRows(alist: list):
    return '\n'.join(alist)

file = open(filename, 'w')
file.write(listToRows(emails))
file.close()