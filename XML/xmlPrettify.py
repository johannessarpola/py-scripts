import sys, os, glob, xml.dom.minidom, codecs
from os import listdir
from os.path import isfile, join

"""
[folder] [ext]
"""
foldername = ''
extension = ''
if(len(sys.argv)>1):
    foldername = os.getcwd() + "\\"+sys.argv[1]
    extension = sys.argv[2]
else:
    foldername = os.getcwd() + "\\output\\"
    extension = 'csv'

os.chdir(foldername)
files = [os.path.abspath(x) for x in glob.glob("*."+extension)] 
for file in files:
    num_lines = sum(1 for line in open(file))
    if num_lines > 0:
        xml = xml.dom.minidom.parse(file) # or xml.dom.minidom.parseString(xml_string)
        pretty_xml_as_string = xml.toprettyxml()
        stringP = os.path.abspath(file)
        opened = codecs.open(stringP, "w", "utf-8")
        opened.write(pretty_xml_as_string)
        opened.close()
print('done')
