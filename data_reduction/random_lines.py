import random
import sys
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', "--input")
parser.add_argument('-o', "--output")
parser.add_argument('-r', "--rows", type=int)

args = parser.parse_args()

input_file = args.input
output_file = args.output
rows = args.rows

def read_random_lines(input_file, rows):
    """
    Read random lines
    """
    with open(input_file, "rb") as input:
        lines = [line for line in input]
    random_choice = random.sample(lines, rows)
    return random_choice


def write(content, output_file):
    with open(output_file, "wb") as sink:
        sink.write(b"".join(content))


content = read_random_lines(input_file, rows)
write(content, output_file)
print("done!")
