import json
"""
Deploys an application from one folder to another in zip
- Has multiple attributes defined in the confinguration json like version number,
files to be included and such
"""

inputKey = 'folder-to-be-deployed'
includedKey = 'includes'
versionKey = 'version'
outputKey = 'folder-deployment'
releasenameKey = 'deployment-name'
# TODO Test from actual deployment to gdrive
# TODO Possibility for a email-agent to let users know a new version is available
# TODO Add some test cases and make it more fail-safe


class DeploymentAgent:
    """
    Class to hold the relevant parameters from json and perform
    the subtasks in deployment
    """
    def __init__(self, jsonConf):
        self.input = self.path_sep_swap(jsonConf[inputKey])
        self.deploymentpath = self.path_sep_swap(jsonConf[outputKey])
        self.releaseName = jsonConf[releasenameKey]
        self.files = jsonConf[includedKey]
        self.version = jsonConf[versionKey]

    def path_sep_swap(self, path):
        from os import sep
        fixed = path
        if '/' in path:
            fixed = fixed.replace("/", sep)
        return fixed

    def get_filePaths(self):
        """
        Gets the actual filenames from the directory defined in the conf and support for wildcards through glob
        """
        from os import path, sep
        import glob
        includedFiles = []
        for filep in self.files:
            if path.isdir(path.join(self.input, filep.replace("/", sep))) or '*' in filep:
                # Recursive search for all files in defined folder
                filep = self.path_sep_swap(filep)
                wildCardQuery = ''
                if '*' not in filep:
                    # If not defined just get all in the folder and subdirectories
                    wildCardQuery = path.join(self.input, filep, '**', '*.*')
                else:
                    wildCardQuery = path.join(self.input, filep)
                print(wildCardQuery)
                for fn in glob.iglob(wildCardQuery, recursive=True):
                    print(fn)
                    includedFiles.append(path.abspath(fn))
            else:
                includeFile = path.join(self.input, filep)
                includedFiles.append(path.abspath(includeFile))
        return includedFiles

    def zipFiles(self):
        import zipfile
        from os import path
        filep = self.get_filePaths()
        zipname = '-'.join([self.releaseName, self.version]) + '.zip'
        deployment = zipfile.ZipFile(zipname, "w")
        for p in filep:
            rootFolder = path.abspath(self.input)
            rp = path.basename(p)
            if(p.startswith(rootFolder)):
                rp = p[len(rootFolder)+1:]
            deployment.write(p, rp, compress_type=zipfile.ZIP_DEFLATED)
        deployment.close()
        return deployment.filename

    def deployZip(self, zipPath):
        """
        Moves the packaged release to a folder
        """
        import shutil
        from os import path, makedirs
        if not path.exists(self.deploymentpath):
            makedirs(self.deploymentpath)
        filename = path.basename(zipPath)  # should return n.ext
        deployment = path.join(self.deploymentpath, filename)
        shutil.move(zipPath, deployment)


def readJson(path):
    with open(path) as data_file:
        data = json.load(data_file)
    return data


def parseConf(data):
    conf = DeploymentAgent(data)
    return conf


def main():
    from sys import argv
    from time import time
    start = time()
    confPath = ''

    if(len(argv) > 1):
        confPath = argv[1]
    else:
        print('pass the conf path in arguments')

    jsonConf = readJson(confPath)
    conf = parseConf(jsonConf)
    payload = conf.zipFiles()
    conf.deployZip(payload)
    deptime = round(time() - start, 2)
    print('Deployment of '+conf.releaseName+' finished in %s' % deptime)


if __name__ == "__main__":
    main()
